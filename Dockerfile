# ubuntu-autopkgtest/Dockerfile

ARG SUITE
FROM ubuntu:${SUITE}
ARG VCS_URL
ARG VCS_REF
ARG SOURCE_DATE_EPOCH
ARG SOURCE_DATE
ARG BUILD_DATE
LABEL \
    org.label-schema.schema-version="1.0" \
    org.label-schema.vcs-url="${VCS_URL}" \
    org.label-schema.vcs-ref="${VCS_REF}" \
    org.label-schema.source-date-epoch="${SOURCE_DATE_EPOCH}" \
    org.label-schema.source-date="${SOURCE_DATE}" \
    org.label-schema.build-date="${BUILD_DATE}"
COPY ./dockerfile-run .
RUN ./dockerfile-run

# ubuntu-autopkgtest/Makefile

suites := $(shell \
  { \
    ubuntu-distro-info --supported ; \
    ubuntu-distro-info --supported-esm ; \
  } | sort | uniq -c | awk '/2/{print $$NF}' | xargs ; \
)
$(info suites: $(suites))

tars := $(patsubst %,ubuntu-autopkgtest.%.tar,$(suites))
$(info tars: $(tars))

.PHONY: all
all: $(tars)

ubuntu-autopkgtest.%.tar: build-images dockerfile-run Dockerfile
	./build-images $(word 2,$(subst ., ,$@))

.PHONY: check
check: shellcheck shfmt yamllint

.PHONY: shellcheck
shellcheck: build-images dockerfile-run
	shellcheck -x $^

.PHONY: shfmt
shfmt: build-images dockerfile-run
	shfmt -i 4 -ci -bn -s -d -w -ln posix $^

.PHONY: yamllint
yamllint: .gitlab-ci.yml .yamllint
	yamllint $^

.PHONY: clean
clean:
	rm -vf *.tar *~
